class Board

  DISPLAY = {
    nil => " ",
    :s => " ",
    :x => "x"
  }

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def self.random
    self.new(self.default_grid, true)
  end

  attr_reader :grid

  def initialize(grid = Board.default_grid, random = false)
    @grid = grid
    randomize if random
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, val)
    row, col = pos
    grid[row][col] = val
  end

  def empty?(pos = nil)
    if pos
      self[pos].nil?
    else
      count == 0
    end
  end

  def full?
    grid.flatten.none?(&:nil?)
  end

  def in_range?(pos)
    pos.all? { |n| n.between?(0, grid.length - 1)}
  end


  def won?
    grid.flatten.none? {|el| el == :s}
  end

  def size
    grid.length
  end

  def randomize(count = 10)
    count.times {place_random_ship}
  end

  def place_random_ship
      raise "Error" if full?
      pos = random_pos
      while full? #full?
        pos = random_pos
      end

      self[pos] = :s
  end

  def random_pos
    [rand(grid.length), rand(grid.length)]
  end


  def count
    grid.flatten.select {|b| b == :s}.length
  end

end
