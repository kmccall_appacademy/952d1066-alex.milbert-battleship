class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new("Rhonda"), board = Board.random)
    @player = player
    @board = board
    @hit = false
  end

  def attack(pos)
    if board[pos] == :s
      @hit = true
    else
      @hit = false
    end
    board[pos] = :x
  end

  def count
    board.count #counting the board
  end

  def game_over?
    board.won?
  end

  def play_turn
    pos = player.get_play
    attack(pos)
  end





end
